 //
 // Created by admin on 2024/5/15.
 //

 #ifndef TESTDUMP_GSWEBSOCKET_H
 #define TESTDUMP_GSWEBSOCKET_H
 #include <iostream>
 #include <functional>
 #include "libwebsockets.h"
 #include <csignal>
 #define MAX_PAYLOAD_SIZE (10*1024)
 struct session_data
 {
     int msg_count;
     size_t len;
     unsigned char buf[LWS_PRE + MAX_PAYLOAD_SIZE];
     bool bin;
     bool fin;
 };
 using GS_WEBSOCKET_CALLBACK = lws_callback_function*;

 class GSWebsocketSever
 {
 public:
     explicit GSWebsocketSever();
     virtual ~GSWebsocketSever();
 public:
     int exit_sig{};
 private:
     struct lws_protocols protocols[2]{};
     struct lws_context_creation_info ctx_info{nullptr};
     struct lws_context* context;
 public:
     void InitServer();
     void StartServer();
     void StopServer();
     void UnInitServer();
     GS_WEBSOCKET_CALLBACK lws_callback{};
     static int protocol_callback( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len );

 public:
     void TestServer();
 };

 struct session_data_client
 {
     int msg_count;
     int len;
     unsigned char buf[LWS_PRE + MAX_PAYLOAD_SIZE];
 };

 class GSWebsocketClient
 {
 public:
     explicit GSWebsocketClient();
     virtual ~GSWebsocketClient();
 private:
     int exit_sig{};
     struct lws_protocols protocols[2]{};
     struct lws_context_creation_info ctx_info = { nullptr };
     struct lws_client_connect_info conn_info { nullptr };
     struct lws_context* context;
     struct lws *wsi;
 public:
     void InitServer();
     void StartServer();
     void StopServer();
     void UnInitServer();
     GS_WEBSOCKET_CALLBACK client_callback{};
     static int client_data_callbakc(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len );
 public:
     void TestServer();
 };

 #endif //TESTDUMP_GSWEBSOCKET_H
