 //
 // Created by admin on 2024/5/16.
 //

 #ifndef UI_LOADQML_H_
 #define UI_LOADQML_H_

 #include <QQmlEngine>
 #include <QQmlApplicationEngine>
 #include <QQmlComponent>
 #include <QQmlContext>
 #include <QQuickView>
 class LoadQml : public QObject
 {
 public:
     explicit LoadQml(QObject* parent = nullptr);
     ~LoadQml()override;
 private:
     QQuickView* view{};
     QQmlApplicationEngine* engine{};
     QQmlComponent* component{};
     QQmlContext* context{};

 public:
     void InitLoader();
     void UnInitLoader();

     QObject* LoadQmlWithComponent(const QString&);
     void LoadQmlWithQuickView(const QString&);

 };


 #endif
