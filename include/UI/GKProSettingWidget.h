 #ifndef GK_SETTING_WIDGET_H
 #define GK_SETTING_WIDGET_H
 #include <QWidget>
 #include <QWizard>
 #include "ui_GKSettingWidget.h"

 QT_BEGIN_NAMESPACE
 namespace Ui
 {
     class SettingWidget;
 }
 QT_END_NAMESPACE

 class GKSettingWizardWidget;

 class GKSettingWidget : public QWidget
 {
     Q_OBJECT
 public:
     Q_PROPERTY(QString userPhoneNum READ GetPhoneNum )
     Q_PROPERTY(QString AthuriCode READ GetAuthriCode )
 public:
     explicit GKSettingWidget(QWidget* parent = nullptr);
     virtual ~GKSettingWidget();
 signals:
     void OnLoginSuccessSignal();
 public slots:
     void OnLoginBtnSlot();
     void OnCancelBtnSlot();
 private:
     QString GetPhoneNum() const;
     QString GetAuthriCode() const;

 private:
     Ui::SettingWidget* ui;
     GKSettingWizardWidget* m_wizard;
 public:
     const char* thisPagePro = "setting";
 private:
     QString m_userPhoneNum;
     QString m_AthuriCode;
 };
 #endif