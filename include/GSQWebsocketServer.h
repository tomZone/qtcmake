 //
 // Created by admin on 2024/5/16.
 //

 #ifndef GSQWEBSOCKETSERVER_H_
 #define GSQWEBSOCKETSERVER_H_
 #include <QtCore/QObject>
 #include <QtCore/QList>
 #include <QtCore/QByteArray>
 #include <QtNetwork/QSslError>
 #include <QtWebSockets/QtWebSockets>

 class GSQWebsocketServer:public QObject
 {
     Q_OBJECT
 public:
     explicit GSQWebsocketServer(QObject* parent= nullptr);
     ~GSQWebsocketServer() override;
 private:
     QWebSocketServer* m_server;
     QList<QWebSocket *> m_clients;
     const int port = 8000;
     QSslConfiguration sslConfiguration;

 private slots:
     void onNewConnectionSlot();
     void onProcessTextMessageSlot(const QString& message);
     void onProcessBinaryMessageSlot(const QByteArray& message);
     void onSocketDisconnectedSlot();
     static void onSslErrorsSlot(const QList<QSslError> &errors);
 public:
     void initServer();
     void startServer();
     void stopServer();
     void unInitServer();
     void TestFunc();
 };



 class SslEchoClient : public QObject
 {
 Q_OBJECT
 public:
     explicit SslEchoClient( QObject *parent = nullptr);

 private Q_SLOTS:
     void onConnected();
     void onTextMessageReceived(const QString& message);
     void onSslErrors(const QList<QSslError> &errors);

 private:
     QWebSocket m_webSocket;
 public:
     void TestFunc();
 };




 #endif
