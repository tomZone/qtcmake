 #ifndef GS_SVG_H
 #define GS_SVG_H

 #include <QObject>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <chrono>
#include <cassert>
#include "matplot/matplot.h"
//#include <windows.h>
 using namespace matplot;

 class GSTestMaplotlib
 {
 public:
     explicit GSTestMaplotlib();
     ~GSTestMaplotlib();
 public:
     void InitAxez();
     void TestFunc();
     std::vector<double> CaculatePower(std::vector<double>& u, std::vector<double>& i);
     void AddIVData(axes_handle&,std::vector<double>& u, std::vector<double>& i);
     void ADDPVData(axes_handle&,std::vector<double>& u, std::vector<double>& i);
     void GetIVPointsData(const char* path,std::vector<double>& x,std::vector<double>& y);
     void GetStringOfSub(std::string src, std::string splitStr, std::vector<std::string>& strVec, int isRFind);

 private:
     axes_handle parent_axes;
     figure_handle parent_figure;
 };





 #endif