import QtQml
import QtQml.Models
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Basic

Item
{
    id:attends
    width:parent.width
    height:40

    RowLayout
    {
        spacing:10
        width:parent.width
        Rectangle
        {
            width:50;height:20
            color: "transparent"
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Row{
                anchors.centerIn: parent
                Text{
                    text:"应到:"
                }
                Text
                {
                    text:"65"
                }
            }
        }

        Rectangle
        {
            width:50;height:20
            color: "transparent"
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Row{
                anchors.centerIn: parent
                Text{
                    text:"应到:"
                }
                Text
                {
                    text:"65"
                }
            }
        }

        Rectangle
        {
            width:50;height:20
            color: "transparent"
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Row{
                anchors.centerIn: parent
                Text{
                    text:"应到:"
                }
                Text
                {
                    text:"65"
                }
            }
        }

    }
}