import QtQml
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Basic

Rectangle
{
    id:weekscore
    width:(parent.width)
    height:110
    // color:"yellow"
    color: "transparent"
    ColumnLayout
    {
        anchors.fill:parent
        anchors.leftMargin:10
        anchors.rightMargin:5
        anchors.topMargin:5
        anchors.bottomMargin:5
        spacing:10
        Text
        {
            text:"护眼积分"
            horizontalAlignment:Text.AlignHCenter
            verticalAlignment:Text.AlignVCenter
            font.family: "黑体"
            font.pixelSize: 20
        }
        RowLayout
        {
            Item {
                Layout.fillWidth: true
            }
            Row
            {
                Image
                {
                    width:30;height:30
                    source:"qrc:/img/child.png"
                }
                UserQmlSpaceItem{}
                Text
                {
                    text:"李"
                    height:30
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    font.family: "黑体"
                    font.pixelSize: 16
                }
            }
            Item {
                Layout.fillWidth: true
            }
            Row
            {
                Image
                {
                    width:30;height:30
                    source:"qrc:/img/child.png"
                }
                UserQmlSpaceItem{}
                Text
                {
                    text:"李"
                    height:30
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    font.family: "黑体"
                    font.pixelSize: 16
                }
            }
            Item {
                Layout.fillWidth: true
            }
            Row
            {
                Image
                {
                    width:30;height:30
                    source:"qrc:/img/child.png"
                }
                UserQmlSpaceItem{}
                Text
                {
                    text:"李"
                    height:30
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    font.family: "黑体"
                    font.pixelSize: 16
                }
            }
            Item {
                Layout.fillWidth: true
            }
        }
        RowLayout
        {
            Item {
                Layout.fillWidth: true
            }
            Row
            {
                Image
                {
                    width:30;height:30
                    source:"qrc:/img/child.png"
                }
                UserQmlSpaceItem{}
                Text
                {
                    text:"李"
                    height:30
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    font.family: "黑体"
                    font.pixelSize: 16
                }
            }
            Item {
                Layout.fillWidth: true
            }
            Row
            {
                Image
                {
                    width:30;height:30
                    source:"qrc:/img/child.png"
                }
                UserQmlSpaceItem{}
                Text
                {
                    text:"李"
                    height:30
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    font.family: "黑体"
                    font.pixelSize: 16
                }
            }
            Item {
                Layout.fillWidth: true
            }
            Row
            {
                Image
                {
                    width:30;height:30
                    source:"qrc:/img/child.png"
                }
                UserQmlSpaceItem{}
                Text
                {
                    text:"李"
                    height:30
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    font.family: "黑体"
                    font.pixelSize: 16
                }
            }
            Item {
                Layout.fillWidth: true
            }
        }

    }

}