import QtQml
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Basic

Rectangle
{
    id:headRect
    // color:"gray"
    color: "transparent"
    width:(parent.width)
    height:50
    signal hideSignal()
    default property var description:"class head layout"
    Item
    {
        id:rectItem
        anchors.fill:parent
        Rectangle
        {
            id:imgRect
            width:30;height:30
            anchors.left: rectItem.left
            anchors.leftMargin:5
            anchors.verticalCenter:rectItem.verticalCenter
            color: "transparent"
            Image
            {
                id:headImg
                anchors.fill:parent
                source:"qrc:/img/icon.png"
                fillMode:Image.PreserveAspectFit
            }
        }

        Text
        {
            id:classText
            height:30
            text:"初一一班"
            horizontalAlignment:Text.AlignHCenter
            verticalAlignment:Text.AlignVCenter
            font.family: "黑体"
            font.pixelSize: 20
            anchors.verticalCenter:rectItem.verticalCenter
            anchors.left:imgRect.right
            anchors.leftMargin:5
        }
        Button
        {
            id:hideBtn
            width:30;height:30
            anchors.right: rectItem.right
            anchors.verticalCenter: rectItem.verticalCenter // 垂直居中对齐
            anchors.rightMargin:5
            Image
            {
                anchors.fill:parent
                source:"qrc:/img/hide.png"
            }
            onClicked:
            {
                console.log("click btn");
                headRect.hideSignal()
            }
        }
    }
    Component.onCompleted:
    {
        hideBtn.clicked.connect(headRect.hideSignal)
    }

}


