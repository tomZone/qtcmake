import QtQml
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Basic
import QtQuick.Window
ApplicationWindow {
    id:mainWindow
    width: 400
    height: 540
    visible: true
    flags: Qt.FramelessWindowHint
    color:"#e5fff3"
    ColumnLayout
    {
        spacing:5
        width:parent.width
        Head{
            id:head
        }
        WeekStar
        {
            id:weekStart
            //anchors.top:head.bottom
        }
        WeekScore
        {
            id:score
            //anchors.top:weekStart.bottom
        }

        TeacherMessage
        {
            id:teacherMsg
            //anchors.top:score.bottom
        }
        Attends
        {
            id:attend
            //anchors.top:teacherMsg.bottom
        }
    }


    Component.onCompleted:
    {
        console.log(head.description)
        head.hideSignal.connect(hideWindow)
        calculateWindowSize()
    }
    function calculateWindowSize()
    {
        mainWindow.x = Screen.width-width
        mainWindow.y = 0
    }
    function hideWindow()
    {
        mainWindow.visible = false;
    }
}

