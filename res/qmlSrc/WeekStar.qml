import QtQml
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Basic

Rectangle
{
    id:weekStars
    width:(parent.width)
    height:100
    // color:"blue"
    color: "transparent"
    ColumnLayout
    {
        id: colum
        anchors.fill:parent
        anchors.leftMargin:10
        spacing:10
        Text
        {
            id:title1
            text:"周护眼明星"
            horizontalAlignment:Text.AlignHCenter
            verticalAlignment:Text.AlignVCenter
            font.family: "黑体"
            font.pixelSize: 20
        }

        RowLayout
        {
            Item {
                Layout.fillWidth: true
            }
            Row
            {
                id:star1
                width:50
                Image
                {
                    width:30;height:30
                    source:"qrc:/img/badge.png"
                    fillMode:Image.PreserveAspectFit
                }
                Text
                {
                    text:"刘"
                    font.family: "黑体"
                    font.pixelSize: 20
                    topPadding:5
                    bottomPadding:5
                    leftPadding:5
                }
            }
            Item {
                Layout.fillWidth: true
            }
            Row
            {
                id:star2
                width:50
                Image
                {
                    width:30;height:30
                    source:"qrc:/img/badge.png"
                    fillMode:Image.PreserveAspectFit
                }
                Text
                {
                    text:"关"
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    font.family: "黑体"
                    font.pixelSize: 20
                    topPadding:5
                    bottomPadding:5
                    leftPadding:5
                }
            }
            Item {
                Layout.fillWidth: true
            }
            Row
            {
                id:star3
                width:50
                Image
                {
                    width:30;height:30
                    source:"qrc:/img/badge.png"
                    fillMode:Image.PreserveAspectFit
                }
                Text
                {
                    text:"张"
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    font.family: "黑体"
                    font.pixelSize: 20
                    topPadding:5
                    bottomPadding:5
                    leftPadding:5
                }
            }
            Item {
                Layout.fillWidth: true
            }
        }
    }

}