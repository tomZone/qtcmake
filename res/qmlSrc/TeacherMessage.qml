import QtQml
import QtQml.Models
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Basic

Item
{
    id:msgRect
    width:parent.width
    height:140
    RowLayout
    {
        id:rowLay
        spacing:10
        width:parent.width
        Text
        {
            text:"消息记录"
            font.family: "黑体"
            font.pixelSize: 20
            Layout.leftMargin: 10
            Layout.bottomMargin: 10
        }
        Item
        {
            Layout.fillWidth: true
        }
        Text
        {
            text:"疑难解答"
            Layout.rightMargin: 10
            Layout.bottomMargin: 10
        }
    }


    Component
    {
        id:myDelegate
        ColumnLayout
        {
            Layout.fillWidth:false
            Row
            {
                Layout.leftMargin: 10
                spacing:10
                Text
                {
                    text:teacName+" 呼叫 "+stuName
                    verticalAlignment:Text.AlignVCenter
                }
            }
            Text
            {
                Layout.leftMargin: 10
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                Layout.preferredWidth: 380          //设置宽度，width设置没有作用
                text:"在进入编程的世界之前，让我们先来思考一下人类思维的奥妙。正如我们生活中的所有事物都是由基本元素组成的，同样，"+
                    "编程中的界面也是由一些基础的布局元素组成的。这些元素的组合和交互构成了我们在屏幕上看到的各种应用和界面"
                font.pointSize: 9
                verticalAlignment:Text.AlignVCenter
                //wrapMode: Text.WordWrap //换行
                elide:Text.ElideRight
            }
        }
    }
    ListModel
    {
        id:myModel
        ListElement{teacName:"王老师";stuName:"小明"}
        ListElement{teacName:"李老师";stuName:"小彭"}
        ListElement{teacName:"李老师";stuName:"小彭"}
        ListElement{teacName:"李老师";stuName:"小彭"}
        ListElement{teacName:"李老师";stuName:"小彭"}
        ListElement{teacName:"李老师";stuName:"小彭"}
    }
    ListView
    {
        id:list
        width:parent.width
        height:100
        anchors.top:rowLay.bottom
        anchors.topMargin:10
        model:myModel
        delegate:myDelegate
        clip:true       //设置滚动不会超出界面
        Timer           //定时器自动滚动
        {
            interval: 1000
            repeat: true
            running: true
            onTriggered: {
                let index = list.currentIndex
                if (index < list.count)
                    list.currentIndex = index + 1
                else
                    list.currentIndex = 0
            }
        }
    }
}

