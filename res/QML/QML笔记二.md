1.Item 
    所有可视化类型的基类
    可以作为容器,里面包含各种可视化类
    1.1 opacity不透明度
        取值范围：0.0-1.0
        父容器的不透明度会影响到子容器,如果要设置不透明度，只需要更改子容器的不透明度即可
    1.2 visible可见与enable启用
        visible 可视化控件的可见性，可以继续接收键盘事件
        enable   可视化控件是否接受事件
    1.3 z堆叠顺序
        按照代码前后实现堆叠 z取值为整数
    1.4 定位子容器和坐标映射
        childAt(x,y)返回(x,y)处的第一个子容器
        mapFromItem(item,x,y)将子容器坐标(x,y)映射到其父容器的坐标上
        mapToItem(item,x,y) 从当前容器映射到item坐标系中

2.Rectangle
    继承Item，使用纯色或渐变色填充的矩形区域，带有边框
    2.1 相关属性
        color属性填充纯色、gradient属性指定一个Gradient类型的渐变，两者都有则以渐变为准
        border属性提供了边框，boder.color、border.width指定颜色和边框宽度
3.Text
    显示纯文本、富文本
    3.1属性
        text 显示文本内容
        
        font 设置文本字体簇 例如
            bold            加粗        true |  false  
            capitalization  大写策略    
                            [Font.MixedCase不变大小写 Font.AllUppercase全部大写 Font.AllLowercase全小写  Font.SmallCaps小型大写字母  Font.Capitalize首字母大写]
            family          字体簇
            pointSize       字号    
            pixleSize       字号  像素  
            italic          斜体            true false
            letterSpacing   字符间距        正值加大间距  负值减小间距
            strikeout       是否有删除线    true false
            underline       是否有下划线    true false
            weight          粗细            1~1000之间
            wordSpace       单词间距        正值加大间距  负值减小间距

        width、height 设置宽高，没有明确设置会自适应

        wrapMode 设置换行 
            Text.NoWrap     不换行
            Text.WordWrap   单词边界换行
            Text.WrapAnywhere  到达边界就会在任一点换行
            Text.Wrap           尽量在单词边界换行
        elide  设置超出宽度的文本缩略显示
            必须显式的设置文本宽度，才会生效
            Text.ElideNone      
            Text.ElideLeft
            Text.ElideMiddle
            Text.ElideRight   设置了maximumLineCount 或者高度，还可以适配可换行的文本
        color  设置文本颜色   
        
        clip 设置文本是否被裁剪，true会裁剪掉与文本边界不服的内容
        
        horizontalAlignment   水平对齐
            Text.AlignLeft
            Text.AlignRight
            Text.AlignHCenter
            Text.AlignJustfy
        verticalAlignmen        垂直对齐
            Text.AlignTop
            Text.AlignBottom
            Text.AlignVCenter
        style 文本样式
            Text.Outline
            Text.Raised
            Text.Sunken
            Text.Normal
        textFormat 文本格式
            Text.AutoText
            Text.PlainText
            Text.StyledText     支持一些基本的html文本样式标签，要求标签必须正确配套
            Text.RichText
            Text.MarkdownText
        Text::onLinkActivated 超链接信号处理器
            必须是富文本或者html格式
        例子
        Item{
            Text{
                textFormat:Text.RichText;font.pointSize:24;
                text:"welcom visit <a href=\"https://qter.org\">Qt</a>";
                onLinkActivated:(link)=>console.log(link+"link actived");
            }
        }

4.TextInput 
    单行可编辑纯文本
    4.1 验证器
        validator:IntValidator{bottom: 10 ;top: 30} 整数验证器 限制输入为10-30的整数
        validator:DoubleValidator{}             非整数验证器
        validator:RegularExpressionValidator{}  正则表达式
    4.2 输入掩码
        inputMask  指定特殊的字符限制输入的格式内容
        inputMask:">AA_9_a"  表示输入两个字母，一个数字和一个可选字母
    4.3 输入完成信号
        onEditingFinished:
    4.4 回显模式
        echoMode属性指定回显模式
            TextInput.Normal        显示文本
            TextInput.Password      密码掩码字符显示
            TextInput.NoEcho        不显示输入内容
            TextInput.PasswordEchoOnEdit    使用密码验码字符，在输入时显示真实字符
    4.5 信号处理器
        onAccepted()        回车键被按下调用
        onEditingFinished   回车键被按下调用，失去焦点也会被调用
        onTextEdited        内容被编辑时调用
    4.6 文本选取
        selectByMouse 使用鼠标选取内容  true false
        selectedText:获取被选中的文本内容
        selectedColor 选取文本的背景色
        selectedTextColor   选取文本的前景色
        selectedStart   获取鼠标选取的文本块前后的光标位置
        selectedEnd   
5. TextEdit
    显示多行可编辑格式化文本
    5.1 focus  设为true  可以接受键盘消息
    5.2 搭配Flickable 实现滚动光标跟随效果


