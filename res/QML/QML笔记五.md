控件和对话框
    使用Qt Quick Controls模块，导入import QtQuick.Controls
    1.Window窗口
        Qt Quick项目都可以将Window作为根对象
        设置 width、height、x、y宽高、坐标
        设置title 窗口标题
        设置color窗口背景色
        设置opacity窗口透明度
        设置visible属性显示隐藏
        设置show()显示窗口
        设置showFullScreen()全屏
        设置showMaximized()最大化
        设置showMinimized()最小化
        设置showNormal()正常显示
        设置hide()、close()隐藏、关闭
        关闭窗口发射closing信号，信号处理器onClosing，在其中设置close.accepted = false，强制窗口保持显示
        window也可以创建在Item中
        设置flags指定窗口显示类型 Qt.Dialog、Qt.Popup
        设置modality指定窗口是否为模态，默认非模态  Qt.NonModal、 模态Qt.WindowModal(阻塞父窗口) Qt.ApplicationModal(阻塞整个应用)
        import QtQQuick
        import QtQQuick.Controls
        Window{
            id:root;
            property bool changed:true
            width:;height:; x:;  y:;            
            visible:true
            title:qsTr("widnow test")
            opacity:0.7
            onClosing:(close)=>{
                if(changed){
                    close.accepted = false
                    dialog.show()
                }
            }
            Window{
                id:dialog
                width:;height:; x:;  y:;
                flags:Qt.Dialog
                modality:Qt.WindowModal
                Label{
                    text:qsTr("确定退出？")
                }
                Row{
                    spacing:10;x120;y80
                    Button{
                        text:qsTr("确定")
                        onClicked:{
                            root.changed = false
                            dialog.close()
                            root.close()
                        }
                    }
                    Button{
                        text:qsTr("取消")
                        onClicked:{
                            dialog.close()
                        }
                    }
                }
            }
        }
    ApplicationWindow应用程序主窗口
        该类为Window的子类，在Window基础上添加了menuBar、header、footer属性
        ApplicationWindow{
            visible:true
            title:qsTr("application")
            menuBar:MenuBar{
                id:menuBar
                Menu{
                    id:fileMenu
                    title:qsTr("文件")
                    MenuItem{
                        title:qsTr("关闭")
                        icon.source:"close.png"
                        onTriggered:close()
                    }
                    MenuSeparator{}

                    MenuItem{
                        title:qsTr("关于")
                        icon.source:"about.png"
                        onTriggered:popup.open()
                    }
                    
                }            
            }
            header:ToolBar{
                RowLayout{
                    anchors.fill:parent
                    ToolButton{
                        text:qsTr("<")
                        visible:footerbar.currentIndex === 0
                        enable:stack.depth>1
                        onClicked:stack.pop()
                    }
                    ToolButton{
                        text:qsTr(">")
                        visible:footerbar.currentIndex === 0
                        enable:stack.depth<3
                        onClicked:stack.push(mainView)
                    }
                    PageIndicator{
                        id:indicator
                        visible:footerbar.currentIndex === 0
                        count:stack.depth
                        currentIndex:stack.depth
                    }
                    Label{
                        text:"工具栏"
                        elide:Label.ElideRight
                        horizontalAlignment:Qt.AlignHCenter
                        verticalAlignment:Qt.AlignVCenter
                        Layout.fillWidth:true
                    }

                    ToolButton{
                        text:qsTr("口")
                        onClicked:popup.open()
                    }
                }
            }
            footer:TabBar{}            
        }

    2.控件基类型 Control
        一般分为10类：按钮、容器、委托、指示器、输入、菜单、导航、弹出、分隔、日期
    2.1按钮
        包含AbstractButton及子类型Button、CheckBox、DelayButton、RadioButton、RoundButton、Switch、ToolButton
        AbstractButton
            是一个抽象控件，提供了按钮通用的功能，但本身无法直接使用
            属性                类型        
            action              Action  设置按钮动作，结合菜单项、工具栏使用
            autoExclusion       bool    是否启用排他性，为true，同一父项可选按钮只能选一个，RadioButton、TabButton默认为true
            autoRepeat          bool    是否在按下按住时重复pressed、release、clicked信号，默认为false，为true不会发射pressAndHold信号
            autoRepeatDelay     int     自动重复初始延迟时间，默认300ms
            autoRepeateInterval int     自动重复间隔时间，默认100ms
            checkable           bool    是否可选中
            checked             bool    是否选中
            display             enumeration
            down                bool    视觉上显示按下
        CheckBox、RadioButton、ButtonGroup
            复选框、单选按钮、按钮组包含一组互斥按钮
        DelayButton、Switch
            可选中按钮，被选中发出信号前有个延迟，防止意外按压
            开关按钮，打开关闭切换
    2.2 容器类控件
        ApplicationWindow        
        HorizontalHeaderView    为TableView提供水平表头
        VerticalHeaderView      为TableView提供垂直表头        
        Pane        继承Control，是面板容器基类，需要手动创建布局，RowLayout或者ColumnLayout
        Page        继承Pane，添加了header、footer属性，一般用在ApplicationWindow中间显示不同页面
        Frame       继承Pane，提供一个边框
        GroupBox    继承Frame
        ScrollView  继承Pane。提供垂直、水平滚动条

        Container   允许动态插入、移除项的基本类型，一般将项目作为Container的子对象进行直接声明，也通过addItem、insertItem、moveItem、removeItem动态管理
        SplitView   用来水平、垂直布局项目，每个项目都有一个可拖动的拆分器
        StackView   将页面放入一个栈，先进后出
        SwipeView   基于滑动的导航模型，有一组页面填充，一次只能显示一页，一般与PageIndicator结合使用
        TabBar      基于选项卡的导航模型一般与SwipeView或者StackView类型结合使用
        ToolBar
    2.3委托类控件
        父类ItemDelegate  标准的视图项，可以用作各种视图的委托
        CheckDelegate、     类似CheckBox的委托
        RadioDelegate、     类似RadioButton的委托
        SwipeDelegate、     实现向左向右滑动，一般用在ListView
        SwitchDelegate、
        TreeViewDelegate

    2.4指示器类控件
        BusyIndicator       显示一个忙碌指示器
        PageIndicator
        ProgressBar         显示一个进度条指示器
        ScrollBar
        ScrollIndicator
    2.5输入类控件
        ComboBox        组合按钮，弹出列表组合框
        Dial            类似拨号旋钮控件
        RangeSlider     范围控制器，from to
        Slider          与RangeSlider类似
        TextArea        多行文本编辑器
        TextField       单行文本编辑器
        Tumbler
        SpinBox
    2.6菜单类控件
    2.7导航类
    2.8弹出类
    2.9分隔类
    2.10 日期类
    2.11 对话框
        ColorDialog
        FileDialog
        FontDialog
        MessageDialog