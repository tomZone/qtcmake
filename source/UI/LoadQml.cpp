 //
 // Created by admin on 2024/5/16.
 //
 #include "LoadQml.h"

 LoadQml::LoadQml(QObject *parent) : QObject(parent)
 {

 }
 LoadQml::~LoadQml()
 {
     UnInitLoader();
 }
 void LoadQml::InitLoader()
 {
     view =new QQuickView;
     //component = new QQmlComponent();
     engine = new QQmlApplicationEngine();
     context = new QQmlContext(engine);
 }
 void LoadQml::UnInitLoader()
 {
    if(view!= nullptr)
    {
        delete view;
        view = nullptr;
    }

     if(engine!= nullptr)
     {
         delete engine;
         engine = nullptr;
     }

     if(context!= nullptr)
     {
         delete context;
         context = nullptr;
     }

     if(component!= nullptr)
     {
         delete component;
         component = nullptr;
     }
 }
 QObject* LoadQml::LoadQmlWithComponent(const QString& qmlUrl)
 {
     if(!engine)
         return nullptr;
     if(!component)
         component = new QQmlComponent(engine,QUrl(qmlUrl));
     qDebug() <<"error:"<< component->errors();
     QObject* obj=component->create();
     return obj;
 }
 void LoadQml::LoadQmlWithQuickView(const QString& qmlUrl)
 {
     if(!view)
         return;
     view->setSource(QUrl(qmlUrl));
     view->show();

 }
