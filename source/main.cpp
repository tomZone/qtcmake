#include <QtWidgets/QApplication>
 #include "LoadQml.h"

int main(int argc,char *argv[])
{
    QApplication a(argc,argv);

    LoadQml qml;
    qml.InitLoader();
    qml.LoadQmlWithComponent("qrc:/qmlSrc/main.qml");

    return QApplication::exec();
}
