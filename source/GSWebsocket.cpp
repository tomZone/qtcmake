 //
 // Created by admin on 2024/5/15.
 //

 #include "GSWebsocket.h"


 GSWebsocketSever::GSWebsocketSever():context(nullptr)
 {

 }

 GSWebsocketSever::~GSWebsocketSever()
 {

 }


 void GSWebsocketSever::InitServer()
 {
     lws_callback = protocol_callback;
     protocols[0] = {"ws", lws_callback, sizeof( struct session_data ), MAX_PAYLOAD_SIZE};
     protocols[1] = {nullptr, nullptr,   0,0};

     ctx_info.port = 8000;
     ctx_info.iface = nullptr; // 在所有网络接口上监听
     ctx_info.protocols = protocols;
     ctx_info.gid = -1;
     ctx_info.uid = -1;
     ctx_info.options = LWS_SERVER_OPTION_VALIDATE_UTF8;

 //    ctx_info.ssl_ca_filepath = "../bin/cert/ca-cert.pem";
 //    ctx_info.ssl_cert_filepath = "../bin/cert/server-cert.pem";
 //    ctx_info.ssl_private_key_filepath = "../bin/cert/server-key.pem";
 //    ctx_info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;

     exit_sig = 0;
 }

 void GSWebsocketSever::UnInitServer()
 {
     exit_sig = 0;
 }

 void GSWebsocketSever::StartServer()
 {
     std::cout<<"signal:"<<exit_sig<<std::endl;
     context = lws_create_context(&ctx_info);
     while ( !exit_sig )
     {
         int err_code = lws_service(context, 1000);
     }

     lws_context_destroy(context);
 }

 void GSWebsocketSever::StopServer()
 {
     if(context != nullptr)
         lws_context_destroy(context);
 }

 int GSWebsocketSever::protocol_callback(struct lws *wsi,enum lws_callback_reasons reason,void *user,void *in,size_t len)
 {
     auto *data = (struct session_data *) user;
     switch ( reason ) {
         case LWS_CALLBACK_ESTABLISHED:       // 当服务器和客户端完成握手后
             printf("Client connect!\n");
             break;
         case LWS_CALLBACK_RECEIVE:           // 当接收到客户端发来的帧以后
             // 判断是否最后一帧
             data->fin = lws_is_final_fragment( wsi );
             // 判断是否二进制消息
             data->bin = lws_frame_is_binary( wsi );
             // 对服务器的接收端进行流量控制，如果来不及处理，可以控制之
             // 下面的调用禁止在此连接上接收数据
             lws_rx_flow_control( wsi, 0 );

             // 业务处理部分，为了实现Echo服务器，把客户端数据保存起来
             memcpy( &data->buf[ LWS_PRE ], in, len );
             data->len = len;
             printf("receive message:%s\n",in);

             // 需要给客户端应答时，触发一次写回调
             lws_callback_on_writable( wsi );
             break;
         case LWS_CALLBACK_SERVER_WRITEABLE:   // 当此连接可写时
             lws_write( wsi, &data->buf[ LWS_PRE ], data->len, LWS_WRITE_TEXT );
             // 下面的调用允许在此连接上接收数据
             lws_rx_flow_control( wsi, 1 );
             break;
     }
     // 回调函数最终要返回0，否则无法创建服务器
     return 0;
 }

 void GSWebsocketSever::TestServer()
 {
     InitServer();
     StartServer();

 //    if(exit_sig == 0)
 //    {
 //        StopServer();
 //        UnInitServer();
 //    }
 }




 GSWebsocketClient::GSWebsocketClient()
 {

 }

 GSWebsocketClient::~GSWebsocketClient()
 {

 }
 void GSWebsocketClient::InitServer()
 {
     client_callback = client_data_callbakc;

     protocols[0] = {"ws", client_callback, sizeof( struct session_data ), MAX_PAYLOAD_SIZE};
     protocols[1] = {nullptr, nullptr,   0,0};

     ctx_info.port = CONTEXT_PORT_NO_LISTEN;
     ctx_info.iface = nullptr;
     ctx_info.protocols = protocols;
     ctx_info.gid = -1;
     ctx_info.uid = -1;

     //ssl支持（指定CA证书、客户端证书及私钥路径，打开ssl支持）
 //    ctx_info.ssl_ca_filepath = "../ca/ca-cert.pem";
 //    ctx_info.ssl_cert_filepath = "./client-cert.pem";
 //    ctx_info.ssl_private_key_filepath = "./client-key.pem";
 //    ctx_info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;

 // 创建一个WebSocket处理器
     context = lws_create_context( &ctx_info );

     char address[] = "127.0.0.1";
     int port = 8000;
     char addr_port[256] = { 0 };
     sprintf(addr_port, "%s:%u", "127.0.0.1", 8000 & 65535 );

     conn_info.context = context;
     conn_info.address = "localhost";
     conn_info.port = 8000;
 //    conn_info.ssl_connection = 1;
     conn_info.path = "/";
     conn_info.host = lws_canonical_hostname( context );
     conn_info.origin = "origin";
     conn_info.protocol = protocols[ 0 ].name;
 }
 #include<sys/time.h>
 void GSWebsocketClient::StartServer()
 {
 //    // 下面的调用触发LWS_CALLBACK_PROTOCOL_INIT事件
 //    // 创建一个客户端连接
     wsi = lws_client_connect_via_info( &conn_info );
     while ( !exit_sig )
     {
         /**
          * 下面的调用的意义是：当连接可以接受新数据时，触发一次WRITEABLE事件回调
          * 当连接正在后台发送数据时，它不能接受新的数据写入请求，所有WRITEABLE事件回调不会执行
          */
         //lws_callback_on_writable( wsi );

         int error = lws_service( context, /* timeout_ms = */ 250 );
         if(error <0)
         {
             _lws_log(LLL_ERR,"lws service err");
             return ;
         }
     }

 //    time_t old = 0;
 //    while( !exit_sig  )
 //    {
 //        struct timeval tv;
 //        gettimeofday( &tv, NULL );
 //
 //        /* Connect if we are not connected to the server. */
 //        if( !wsi && tv.tv_sec != old )
 //        {
 //            wsi = lws_client_connect_via_info(&conn_info);
 //        }
 //
 //        if( tv.tv_sec != old )
 //        {
 //            /* Send a random number to the server every second. */
 //            lws_callback_on_writable( wsi );
 //            old = tv.tv_sec;
 //        }
 //
 //        lws_service( context, /* timeout_ms = */ 250 );//在V3.2之后,timeout的值已经废弃,底层自己调度;所以这个地方怎么控制频率,不清楚
 //    }

     // 销毁上下文对象
     lws_context_destroy( context );
 }
 void GSWebsocketClient::StopServer()
 {

 }
 void GSWebsocketClient::UnInitServer()
 {

 }
 int GSWebsocketClient::client_data_callbakc(struct lws *wsi,
                                             enum lws_callback_reasons reason,
                                             void *user,
                                             void *in,
                                             size_t len)
 {
     auto *data = (struct session_data *) user;
     switch ( reason ) {
         case LWS_CALLBACK_CLIENT_ESTABLISHED:   // 连接到服务器后的回调
             lwsl_notice( "Connected to server ok!\n" );
             break;

         case LWS_CALLBACK_CLIENT_RECEIVE:       // 接收到服务器数据后的回调，数据为in，其长度为len
             lwsl_notice( "Rx: %s\n", (char *) in );
             break;
         case LWS_CALLBACK_CLIENT_WRITEABLE:     // 当此客户端可以发送数据时的回调
             if ( data->msg_count < 3 )
             {
                 // 前面LWS_PRE个字节必须留给LWS
                 memset( data->buf, 0, sizeof( data->buf ));
                 char *msg = (char *) &data->buf[ LWS_PRE ];
                 data->len = sprintf( msg, "你好 %d", ++data->msg_count );
                 lwsl_notice( "Tx: %s\n", msg );
                 // 通过WebSocket发送文本消息
                 lws_write( wsi, &data->buf[ LWS_PRE ], data->len, LWS_WRITE_TEXT );
             }
             break;
     }
     return 0;
 }
 void GSWebsocketClient::TestServer()
 {
     InitServer();
     StartServer();
 }
