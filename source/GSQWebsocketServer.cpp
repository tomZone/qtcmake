 //
 // Created by admin on 2024/5/16.
 //
 #include "GSQWebsocketServer.h"
 #include <QtCore/QFile>
 #include <QtNetwork/QSslCertificate>
 #include <QtNetwork/QSslKey>

 GSQWebsocketServer::GSQWebsocketServer(QObject* parent):QObject(parent),m_server(nullptr)
 {

 }
 GSQWebsocketServer::~GSQWebsocketServer() {}

 void GSQWebsocketServer::onNewConnectionSlot()
 {
     QWebSocket *pSocket = m_server->nextPendingConnection();

     qDebug() << "Client connected:" << pSocket->peerName() << pSocket->origin();

     connect(pSocket, &QWebSocket::textMessageReceived,
             this, &GSQWebsocketServer::onProcessTextMessageSlot);
     connect(pSocket, &QWebSocket::binaryMessageReceived,
             this, &GSQWebsocketServer::onProcessBinaryMessageSlot);
     connect(pSocket, &QWebSocket::disconnected,
             this, &GSQWebsocketServer::onSocketDisconnectedSlot);

     m_clients << pSocket;
 }
 void GSQWebsocketServer::onProcessTextMessageSlot(const QString& message)
 {
     auto *pClient = qobject_cast<QWebSocket *>(sender());
     if (pClient)
     {
         pClient->sendTextMessage(message);
     }
 }
 void GSQWebsocketServer::onProcessBinaryMessageSlot(const QByteArray& message)
 {
     auto *pClient = qobject_cast<QWebSocket *>(sender());
     if (pClient)
     {
         pClient->sendBinaryMessage(message);
     }
 }
 void GSQWebsocketServer::onSocketDisconnectedSlot()
 {
     qDebug() << "Client disconnected";
     auto *pClient = qobject_cast<QWebSocket *>(sender());
     if (pClient)
     {
         m_clients.removeAll(pClient);
         pClient->deleteLater();
     }
 }
 void GSQWebsocketServer::onSslErrorsSlot(const QList<QSslError> &errors)
 {
     qDebug() << "Ssl errors occurred";
 }

 void GSQWebsocketServer::initServer()
 {
     m_server = new QWebSocketServer(QStringLiteral("SSL Echo Server"),
                                     QWebSocketServer::SecureMode,
                                     this);

     QFile certFile(QStringLiteral("./cert/localhost.cert"));
     bool ret =  certFile.open(QIODevice::ReadOnly);
     qDebug() << "ret:"<<ret;
     QFile keyFile(QStringLiteral("./cert/localhost.key"));
     ret = keyFile.open(QIODevice::ReadOnly);
     qDebug() << "ret1:"<<ret;

     QSslCertificate certificate(&certFile, QSsl::Pem);
     QSslKey sslKey(&keyFile, QSsl::Rsa, QSsl::Pem);
     certFile.close();
     keyFile.close();

 //    sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
 //    sslConfiguration.setLocalCertificate(certificate);
 //    sslConfiguration.setPrivateKey(sslKey);
 //
 //    m_server->setSslConfiguration(sslConfiguration);
 }
 void GSQWebsocketServer::startServer()
 {
     if (m_server->listen(QHostAddress::Any, port))
     {
         qDebug() << "SSL Echo Server listening on port" << port;
         connect(m_server, &QWebSocketServer::newConnection,this, &GSQWebsocketServer::onNewConnectionSlot);
         connect(m_server, &QWebSocketServer::sslErrors,this, &GSQWebsocketServer::onSslErrorsSlot);
     }
 }
 void GSQWebsocketServer::stopServer()
 {
     m_server->close();
     qDeleteAll(m_clients.begin(), m_clients.end());
 }
 void GSQWebsocketServer::unInitServer()
 {
     if(m_server != nullptr)
     {
         delete m_server;
         m_server = nullptr;
     }
 }

 void GSQWebsocketServer::TestFunc()
 {
     initServer();
     startServer();
 }

 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 SslEchoClient::SslEchoClient(QObject *parent) : QObject(parent)
 {
 //    connect(&m_webSocket, &QWebSocket::connected, this, &SslEchoClient::onConnected);
 //    connect(&m_webSocket, QOverload<const QList<QSslError>&>::of(&QWebSocket::sslErrors),
 //            this, &SslEchoClient::onSslErrors);
 //
 //    QSslConfiguration sslConfiguration;
 //    QFile certFile(QStringLiteral(":/localhost.cert"));
 //    certFile.open(QIODevice::ReadOnly);
 //    QSslCertificate certificate(&certFile, QSsl::Pem);
 //    certFile.close();
 //    sslConfiguration.addCaCertificate(certificate);
 //    m_webSocket.setSslConfiguration(sslConfiguration);
 //
 //    m_webSocket.open(QUrl(url));
 }
 //! [constructor]

 //! [onConnected]
 void SslEchoClient::onConnected()
 {
     qDebug() << "WebSocket connected";
     connect(&m_webSocket, &QWebSocket::textMessageReceived,
             this, &SslEchoClient::onTextMessageReceived);
     m_webSocket.sendTextMessage(QStringLiteral("Hello, world!"));
 }
 //! [onConnected]

 //! [onTextMessageReceived]
 void SslEchoClient::onTextMessageReceived(const QString& message)
 {
     qDebug() << "Message received:" << message;
     qApp->quit();
 }

 void SslEchoClient::onSslErrors(const QList<QSslError> &errors)
 {
     qWarning() << "SSL errors:" << errors;

     qApp->quit();
 }


 void SslEchoClient::TestFunc()
 {
 //    initServer();
 //    startServer();

     QUrl url;
     url.setScheme(QStringLiteral("wss"));
     url.setHost("localhost");
     url.setPort(8000);


     connect(&m_webSocket, &QWebSocket::connected, this, &SslEchoClient::onConnected);
     connect(&m_webSocket, QOverload<const QList<QSslError>&>::of(&QWebSocket::sslErrors),
             this, &SslEchoClient::onSslErrors);

 //    QSslConfiguration sslConfiguration;
 //    QFile certFile(QStringLiteral("./cert/localhost.cert"));
 //    certFile.open(QIODevice::ReadOnly);
 //    QSslCertificate certificate(&certFile, QSsl::Pem);
 //    certFile.close();
 //    sslConfiguration.addCaCertificate(certificate);
 //    m_webSocket.setSslConfiguration(sslConfiguration);

     m_webSocket.open(QUrl(url));
 }


