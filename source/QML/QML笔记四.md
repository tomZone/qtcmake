事件处理
1.MouseArea 
    一般用来和一个可见的控件配合，提供鼠标处理
    enable:
        设置是否启用鼠标处理
    pressed只读属性
        用户是否在MouseArea上按住了鼠标按钮
    containsMouse只读属性
        当前鼠标光标是否在MouseArea上，默认只有鼠标的一个按钮被按下时才可以被检测到
    
    获取鼠标按钮点击
        onClicked()、onDoubleClicked()、onPressed()、onReleased()、onPressAndHold()、onWheel()
    获取鼠标位置
        设置hoverEnabled为true，之后可以获取鼠标位置，且containsMouse只读属性也可以在鼠标按钮没有按下的情况下检测到鼠标
        onPositionChanged()、onEntered()、onExited()
    注：
        当MouseArea与其他MouseArea重叠时，设置 propagateComposedEvents为 true 来传递 clicked doubleClicked 、pressAndHold等事件
        在MouseArea没接受事件时，才可以继续传递，即当一个事件在MouseArea中处理，需要在其他事件处理器中设置 MouseEvent.acepted=false，该事件才能继续传播
    例子
    Rectangle{
        width:100;height:100;
        color:"yellow"
        MouseArea{            
            anchors.fill: parent    //使MouseArea填充整个yellow  Rectangle
            onClicked:{parent.color='red';console.log(" click")}     
            onDoubleClicked:console.log("double click")                  
        }
        Rectangle{
            width:50;height:50;
            color:"blue"
            MouseArea{            
            anchors.fill: parent    //使MouseArea填充整个 blue  Rectangle
            
            propagateComposedEvents:true    //设置为true，保证事件能传递
            
            onClicked:(mouse)=>{
                console.log(" click blue") 
                mouse.acepted = false           //设置为false，该单击事件才会传递到yellow层
            }     
            
            onDoubleClicked:(mouse)=>{
                console.log(" double click blue")   
                mouse.acepted = false           //设置为false，该双击事件才会传递到yellow层
            }     
        }
        }
    }

2.鼠标事件MouseEvent、滚轮事件WheelEvent
    可视化的控件配合MouseArea获取鼠标相关的事件，通过信号与处理对数表进行交互
    MouseArea包含一个mouse参数，该参数类型为MouseEvent，在该参数对象中设置accepted为true，防止鼠标事件传递到下层
    根据 x、y获取鼠标位置，根据button或buttons获取按下的按键
    button 和 buttons的取值Qt.LeftButton、Qt.RightButton、Qt.MiddleButton
    根据modifiers获取按下的键盘修饰符
    modifiers的值是由多个键位组成的
    常用的有 Qt.NoModifier、没有修饰键被按下
            Qt.ShiftModifier、Shift键被按下
            Qt.ControlModifier、CCtrl键被按下
            Qt.AltModifier、Alt键被按下
            Qt.MetaModifier、Meta键被按下
            Qt.KeypadModifier 小键盘键被按下
    例子:
        Rectangle{
            width: 100;height:100;
            color:"green"
            Text{
                id:myText;                
                anchors.centerIn: parent;
                text:"Qt";
                
            }
            MouseArea{
                anchors.fill:parent
                acceptedButtons:Qt.LeftButton|Qt.RightButton 
                onClicked:(mouse)=>{
                    if(mouse.button === Qt.RightButton)
                        parent.color='blue'
                    else
                        parent.color='red'
                }
                onDoubleClicked:(mouse)=>{
                    if((mouse.button===Qt.LeftButton) && (mouse.modifiers===Qt.ShiftModifier))
                        parent.color='green'
                }
                onWheel:(wheel)=>{
                    if(wheel.modifiers & Qt.ControlModifier){
                        if(wheel.angleDelta.y>0)
                            myText.font.pointSize +=1;
                        else
                            myText.font.pointSize -=1;
                    }
                }
            }
        }

3.拖放事件DragEvent
    实现拖放，需要使用MouseArea中的  drag属性
    drag.target         指定要拖动的控件id
    drag.active         指定目标控件当前是否可以被拖动  true false
    drag.axis           指定拖动方向        Drag.XAxis水平方向  Drag.YAxis垂直方向  Drag.XAndAxis水平和垂直方向
    drag.minimumX       水平方向最小拖动距离
    drag.minimumY       垂直方向最小拖动距离
    drag.maximumX       水平方向最大拖动距离
    drag.maximumY       垂直方向最大拖动距离
    drag.filterChildren 使子MouseArea也启用拖动     true false
    drag.smoothed       是否平滑拖动                true false
    drag.threshold      启动拖动阈值，超过该值才被认为是一次拖动

    实现复杂的拖拽事件用到DragEvent，通过x、y获取拖动的位置，使用keys识别数据类型和源的键列表
    通过hasColor、hasHtml、hasText、hasUrls确定拖动类型
    通过colorData、html、text、urls获取具体的数据类型
    formats属性获取拖动数据中包含的MIME类型格式列表
    drag.source获取拖动事件源

    DropArea是一个不可见区域，
        当控件拖到上面时，可以接收相关事件
        通过drag.x drag.y获取拖放事件坐标，
        drag.source获取拖放源对象
        keys获取键列表
        当DropArea中有控件被拖进来时，调用 onEntered，
        当有drop事件时，调用onDropped，
        当拖放离开时，调用onExited
        当拖放位置改变时调用onPositionChanged

4.键盘事件KeyEvent、焦点作用域FocusScope
    键盘事件是在键盘上按下一个键时就触发，控件的focus设置为true，该控件便有焦点
    4.1按键处理
        按下键后，Qt获取键盘动作并产生一个键盘事件
        若window是活动窗口，将键盘事件传递过去
        控件有焦点得到该键盘事件，没有焦点则忽略事件。若焦点控件接受了键盘事件，则事件传播到此结束
    4.2导航键
        KeyNavigation福附加属性，使用方向键或Tab键进行导航
        属性有  backtab(shift+Tab)  down  left  priority right tab up
    4.3查询活动焦点
        通过activeFocus属性进行查询
    4.4获取焦点和焦点作用域
        通过focus 设为 true来使其获得焦点
        通过 FocusScope创建焦点作用域,FocusScope是不可视的，他的子项目需要将属性暴露给FocusScope的父项目中
        例子：
            FocusScope{
                width: rect.width
                height:recct.height
                Rectangle{
                    id: rect
                    anchors.centerIn:parent
                    focus:true                     
                }
            }

5.定时器
    定时器运行时改变其值，经过的时间会被重置
    例如1000ms的定时器，经过了500ms，突然去改变repeat值，则之前的500ms会重置为0，再过1000ms后才能触发
    Item{
        Timer{
            interval:1000       //1000ms执行一次
            running:true           //为true开启定时器
            repeat:true             //为true时重复触发，false为才触发一次
            onTriggered:        //定时器触发时执行这个信号差处理器
                time.text= Date().toString()
            
        }
        Text{id:time}
    }

6.Loader动态加载组件
    6.1 Loader动态加载QML组件，可以加载一个文件或者组件对象
        主要用于延迟组件的创建
        Item{
            width:100
            height:100
            Loader{id:pageLoader}
            MouseArea{
                anchors.fill:parent
                onCliked:pageLoader.sourcce ="Page.qml"
            }
        }

    6.2Loader加载可视化控件时会使用大小规则
        若没有明确指定loader的大小，则loader将会在组建加载完成后自动设置为组件的大小
        若设置了width、height或者使用锚确定了Loader的大小，那么被加载的控件会适配Loader的大小
        Item{
            width:100;height:100;
            Loader{            
                anchors.fill: parent            
                //anchors.centerIn: parent  移除锚定，则显示在父Item中间
                sourceComponent:rect
            }
            Component{
                id:rect
                Rectangle{
                    width:50;height:50;
                }
            }
        }
    6.3接收信号
        从被加载的形目中发射的信号，可以用Connections进行接收
        Item{
            Loader{
                id:myLoader
                source:"MyItem.qml"
            }
            Connections{
                function onMessage(msg){console.log(msg)}
                target: myLoader.item
            }
        }

        Rectangle{
            id:myItem            
            signal message(string msg)
            MouseArea{
                anchors.fill:parent 
                onClicked:myItem.message("111")
            }
            
        }